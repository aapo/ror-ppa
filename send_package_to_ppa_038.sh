#/bin/bash
#Run this at rootdirectory of package and give minor_version as parameter
#  hint: echo "DEBUILD_TGZ_CHECK=no" >> ~/.devscripts
#   (unless it will ask several times to confirm y/n)
#
#example:
#ls
#> send_package_to_ppa.sh
#> hello/debian
#> hello/src
#> foobar/debian
#> foobar/src
#
#cd hello
#../send_package_to_ppa.sh 1
#cd ../foobar
#../send_package_to_ppa.sh 2
#


#Change these:
maintainer="Aapo Rantalainen <aapo.rantalainen@gmail.com>"
address="ppa:aapo-rantalainen/ror-038-stable"



#This is added to changelog, could be empty
additional_message="(For Ubuntu PPA)"
minor_version="$1" #give this as parameter

#Target releases:
release_list=( oneiric precise quantal raring )
#release_list=( precise )

date="`date -R`"


cd debian
#remove old "changelog_" - files
rm -f changelog_*
#backup original changelog
mv changelog changelog_original

#check first line of changelog
temp=( `head changelog_original -n1` )

#package name is first word in changleog
package=${temp[0]}

#second word is version number but it has parenthesis ()
ver_tmp=${temp[1]}

#remove first and last character
version=${ver_tmp:1:${#ver_tmp}-2}

message="  * "

cd ..

for release in "${release_list[@]}"
do

cd debian

echo $package \("$version""$minor_version"+"$release"\) $release\; urgency=low >changelog
echo '' >>changelog
echo "$message" "$additional_message" >>changelog
echo '' >>changelog
echo ' -- '"$maintainer"'  '"$date"  >>changelog
echo '' >>changelog
cat changelog_original >> changelog

cd ..
debuild -S -sa

cd ..
dput -f "$address" "$package"_"$version""$minor_version"+"$release"_source.changes
cd -

#backup used changelogs, this helps debugging
#cd debian
#mv changelog changelog_"$release"
#cd ..
done

#copy changelog back, so this script can be runned second time too
cd debian
mv changelog_original changelog

